use chrono::prelude::*;
use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
pub enum LKCError {
    // General error codes
    #[error("Empty data error")]
    ErrEmptyData,
    #[error("Bad value")]
    ErrBadValue,
    #[error("Not found")]
    ErrNotFound,
    #[error("Not initialized")]
    ErrNoInit,
    #[error("Type mismatch")]
    ErrTypeMismatch,
    #[error("Out of range")]
    ErrOutOfRange,

    #[doc(hidden)]
    #[error("Function unimplemented")]
    ErrUnimplemented,

    /// Program exceptions are also extremely bad, but also highly unlikely thanks to Rust
    #[error("Program exception: {0}")]
    ErrProgramException(String),

    // Keycard error codes
    #[error("Unsupported field")]
    ErrUnsupportedField,
    #[error("Unsupported hash type")]
    ErrUnsupportedHashType,

    #[error("Missing field {0}")]
    ErrMissingField(String),
    #[error("Bad value for field: {0}")]
    ErrBadFieldValue(String),

    #[error("Invalid keycard")]
    ErrInvalidKeycard,
    #[error("Invalid key")]
    ErrInvalidKey,

    #[error("Hash mismatch")]
    ErrHashMismatch,
    #[error("Verification failure")]
    ErrVerificationFailure,
    // Returned when attempting to add a signature to a keycard out of the required order, e.g.
    // adding a Custody signature anywhere but first.
    #[error("Out-of-order signature")]
    ErrOutOfOrderSignature,

    #[error(transparent)]
    EzNaclError(#[from] eznacl::EzNaclError),
}

/// Returns a string containing the current UTC with second precision in the format
/// YYYYMMDDTHHMMSSZ.
pub fn get_timestamp() -> String {
    let utc: DateTime<Utc> = Utc::now();
    let formatted = utc.format("%Y-%m-%dT%H:%M:%SZ");

    String::from(formatted.to_string())
}
