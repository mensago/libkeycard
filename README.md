# libkeycard

A library written in Rust for interacting with keycards on the [Mensago](https://mensago.org) platform released under the Mozilla Public License 2.0.

## Description

This library enables developers to create, sign, and verify keycards, a form of digital certificate designed specifically for the needs of the platform.

## Status

libkeycard is in beta. There may be unforeseen changes in the API and it should not be considered finalized until the 1.0 release, so developers utilizing this library should be aware that there may be breaking changes even between the current development versions, as semver will not be utilized until the 1.0 release. Reading the [CHANGELOG](https://gitlab.com/mensago/libkeycard/-/blob/main/CHANGELOG.md) is highly encouraged when upgrading.

## Usage

Full documentation on how to use the library can be found at its [Crates.io page](https://docs.rs/libkeycard/0.1.12/libkeycard/index.html). The development version in the tree has seen a few API updates since this version, but the changes are minor if changing from stable to development.

## Contributing

Although a mirror repository can be found on GitHub for historical reasons, the official repository for this project is on [GitLab](https://gitlab.com/mensago/libkeycard). Please submit issues and pull requests there.

Mensago itself is a very young, very ambitious project that needs help in all sorts of areas -- not just writing code. Find out more information at https://mensago.org/develop.

## Building

Building libkeycard requires the Rust toolchain. Check out the repository and run `cargo build`.
